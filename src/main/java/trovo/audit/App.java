package trovo.audit;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import trovo.audit.common.Util;
import trovo.audit.service.AuditService;

public class App 
{
	private final static Logger LOGGER = Logger.getLogger(App.class.getName());
	
    public static void main(String[] args )
    {
    	for (;;) {
        	try {
				AuditService.start(args);
				TimeUnit.SECONDS.sleep(Util.getRandomNumber(4, 7));
			} catch (Exception e) {
				LOGGER.info("<<<<<<<<<<<<<FAILED MAIN APP>>>>>>>>>>>");
			}
		}
    }
}
