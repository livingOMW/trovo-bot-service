package trovo.audit.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadConfigFile {

	private Properties properties;
	
	public ReadConfigFile() throws FileNotFoundException, IOException {
		this.properties=new Properties();
        properties.load(this.getClass().getResourceAsStream("/trovo.properties"));
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
