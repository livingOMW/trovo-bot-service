package trovo.audit.service;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.pramcharan.wd.binary.downloader.WebDriverBinaryDownloader;
import io.github.pramcharan.wd.binary.downloader.enums.Browser;
import trovo.audit.common.Util;
import trovo.audit.config.ReadConfigFile;


public class AuditService {
	
	private final static Logger LOGGER = Logger.getLogger(AuditService.class.getName());
	
	public static void start(String[] args) {
		WebDriverBinaryDownloader.create().downloadLatestBinaryAndConfigure(Browser.FIREFOX);
        WebDriver driver = new FirefoxDriver();
        try {
        	ReadConfigFile config = new ReadConfigFile();
			driver.get(config.getProperties().getProperty("trovo.web"));
			driver.manage().window().maximize();
	        Thread.sleep(Util.getRandomMilliseconds(9,12));
	        
	    	WebElement acceptCookies = driver.findElement(By.cssSelector("button[class='cat-button button normal primary']"));
	    	acceptCookies.click();
	    	Thread.sleep(Util.getRandomMilliseconds(1,2));
	    	
	    	if(args.length>0) {
	    		loginTrovo(driver,args);
				Thread.sleep(Util.getRandomMilliseconds(8,10));
	    	}
	    	
			List<WebElement> seeUsers= driver.findElements(By.cssSelector("svg[class='svg-icon cursor-pointer hover-highlight']"));
			seeUsers.get(0).click();
			
			Thread.sleep(Util.getRandomMilliseconds(1,2));
			
			getAllUsers(driver);
			
			Thread.sleep(Util.getRandomMilliseconds(1,2));
		    List<WebElement> allUsers= driver.findElements(By.className("rank-item-user"));
		    List<String> users = allUsers.stream()
		    		.map(u->u.findElement(By.className("user-name")).getText())
		    		.collect(Collectors.toList());
		    
		    SaveService.save(users,config);
		} catch (Exception e) {
			LOGGER.info("<<<<<<<<<<<<<<<<<FAILED DOM ACCESS>>>>>>>>>>>>>>");
		} finally {
            driver.quit();
        }
	}

	/**
	 * Method which will get all users to the DOM of trovo
	 * You must to do several times scroll down and scroll up on the users area
	 * @param driver firefox browser
	 * @throws InterruptedException
	 */
	private static void getAllUsers(WebDriver driver) throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		int usersFirstScroll = 0;
		int usersSecondScroll = 0;
		for (int i = 0; i < 20; i++) {
			List<WebElement> usuarios= driver.findElements(By.className("rank-item-user"));
			usersFirstScroll=usuarios.size();
			//Scroll down
		    js.executeScript("arguments[0].scrollIntoView();", usuarios.get(usuarios.size()-1));
		    Thread.sleep(Util.getRandomMilliseconds(1,2));
		    //Scroll up
		    js.executeScript("arguments[0].scrollIntoView();", usuarios.get(0));
		    Thread.sleep(Util.getRandomMilliseconds(1,2));
		    js.executeScript("arguments[0].scrollIntoView();", usuarios.get(usuarios.size()-1));
		    List<WebElement> usuarios2= driver.findElements(By.className("rank-item-user"));
		    usersSecondScroll=usuarios2.size();
		    if(i>=2 && usersFirstScroll==usersSecondScroll) {
		    	break;
		    }
		}
	}

	/**
	 * Method which just do login to the trovo with args
	 * @param driver firefox browrer driver
	 * @param args trovo login credentials
	 */
	private static void loginTrovo(WebDriver driver, String[] args){
		try {
			String login = args[0];
			String pass = args[1];
			Thread.sleep(Util.getRandomMilliseconds(1, 2));
			WebElement loginButton = driver.findElement(By.cssSelector("div[class='login-btn-wrap']"));
			loginButton.click();
			Thread.sleep(Util.getRandomMilliseconds(3, 4));
			List<WebElement> loginAndEmailButtons=driver.findElement(By.className("content-left"))
					.findElements(By.cssSelector("[class='input-box border-bottom']"));
			WebElement loginInput = loginAndEmailButtons.get(0).findElement(By.cssSelector("input"));
			WebElement passInput = loginAndEmailButtons.get(2).findElement(By.cssSelector("input"));
			loginInput.sendKeys(login);
			passInput.sendKeys(pass+ Keys.ENTER);
			LOGGER.info("<<<<<<<<<<<LOGIN OK>>>>>>>>>>");
		} catch (Exception e) {
			LOGGER.info("<<<<<<<<<<<FAILED login KO>>>>>>>>>>");
		}
	}

}
