package trovo.audit.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import trovo.audit.config.ReadConfigFile;

public class SaveService {
	
	private final static Logger LOGGER = Logger.getLogger(SaveService.class.getName());
	   
	public static void save(List<String> users, ReadConfigFile config) {
		Properties dbConfig =config.getProperties();
		try (Connection conn = DriverManager.getConnection(
				dbConfig.getProperty("db.url"), dbConfig.getProperty("db.user"), dbConfig.getProperty("db.pasword"));
				PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO USERS (NAME) VALUES(?)")) {
			LOGGER.info("<<<<<<<<<<<Started process to save users>>>>>>>>>>");
			conn.setAutoCommit(false);
			for (int i = 0; i < users.size(); i++) {
				preparedStatement.setString(1, users.get(i));
				preparedStatement.addBatch();
			}
			int[] savedUsers = preparedStatement.executeBatch();
			conn.commit();
			conn.setAutoCommit(true);
			LOGGER.info("<<<<<<<<<<<<<<<<<<Saved all users OK TOTAL USERS SAVED:"+savedUsers.length+">>>>>>>>>>>>>>>>>>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.info("<<<<<<<<<<<FAILED save users KO>>>>>>>>>>");
		}
	}

}
