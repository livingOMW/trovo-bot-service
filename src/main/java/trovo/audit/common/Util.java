package trovo.audit.common;

public class Util {
	
	public static int getRandomMilliseconds(int min, int max) {
		   return (int)(((Math.random() * (max + 1 - min)) + min)*1000);
	}
	
	public static int getRandomNumber(int min, int max) {
		   return (int)((Math.random() * (max + 1 - min)) + min);
	}
}
